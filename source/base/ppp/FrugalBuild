# Compiling Time: 0.01 SBU
# Maintainer: James Buren <ryuo@frugalware.org>
# Contributor: Miklos Vajna <vmiklos@frugalware.org>

pkgname=ppp
pkgver=2.4.7
pkgrel=6
pkgdesc="The PPP (Point-to-Point Protocol) daemon"
url="http://www.samba.org/ppp/"
groups=('base')
archs=('x86_64')
depends=('glibc>=2.29-5' 'openssl>=1.1.1-2')
backup=('etc/ppp/chap-secrets' 'etc/ppp/pap-secrets' 'etc/ppp/options' 'etc/ppp/ip-up' 'etc/ppp/ip-down')
up2date="curl -s https://www.samba.org/ftp/ppp/ | grep -o '\(.*\)-\(.*\).tar.gz' | sed 's/.*>//g' | tail -n1 | sed 's/.*-\(.*\).tar.*/\1/'"
source=(http://www.samba.org/ftp/ppp/ppp-$pkgver.tar.gz ip-up ip-down \
	fix-linux-vs-glibc-headers-compile-error-mess.patch CVE-2015-3310.patch \
	ppp-2.4.7-DES-openssl.patch)
sha1sums=('808b023172ea7189bc0d49935bf37a5382a1fe13' \
          '52b2e62a47050498846aa769495628da2a00fb31' \
          '09472529dfcf1f47d7e7f65067291f651c7ac476' \
          '9a1f7fea8bf672f6e7d0289bc6fa1d8a783cda76' \
          '16f2756d21e64bf324e148a9d9c341b8b37bcf21' \
          '7e7e09167bb8a4e7a4ad6eb6d9c368527afbb192')
options+=('nolto')

build()
{
	Fpatchall

	CFLAGS="$CFLAGS -D_GNU_SOURCE"

	# begin patching - CFLAGS and the last one for enabling active filter
	Fsed "-O2 -pipe -Wall -g" "${CFLAGS}" pppd/Makefile.linux
	Fsed "-g -O2" "${CFLAGS}" pppd/plugins/Makefile.linux
	Fsed "-g" "${CFLAGS}" pppd/plugins/rp-pppoe/Makefile.linux
	Fsed "-O2" "${CFLAGS}" pppstats/Makefile.linux
	Fsed "-O2 -g -pipe" "${CFLAGS}" chat/Makefile.linux
	Fsed "-O" "${CFLAGS}" pppdump/Makefile.linux
	Fsed "^#FILTER=y" "FILTER=y" pppd/Makefile.linux
	Fsed "^#HAVE_INET6=y" "HAVE_INET6=y" pppd/Makefile.linux
	Fexec rm -f include/linux/if_pppol2tp.h || Fdie
	# end of patching

	Fmake --prefix=$Fprefix
	make DESTDIR=$Fdestdir/usr install || Fdie
	Frm /usr/lib/pppd/$pkgver/rp-pppoe.so
	Fmkdir /etc/ppp/peers
	Fexe ip-{up,down} /etc/ppp/
	Finstallrel 600 etc.ppp/* /etc/ppp/
	cd scripts
	Fexerel /usr/sbin/pon
	Fexerel /usr/sbin/poff
	Fmanrel pon.1
}

# optimization OK
